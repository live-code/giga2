import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {LanguageService} from "../core/services/language.service";
import {HttpClient} from "@angular/common/http";
import {DEFAULT_LANGUAGE} from "../app.module";
import { ColorPickerComponent } from './forms/color-picker/color-picker.component';
import { RateComponent } from './forms/rate/rate.component';
import { MyInputComponent } from './forms/my-input/my-input.component';
import {ReactiveFormsModule} from "@angular/forms";
import { AnagraficaComponent } from './forms/anagrafica/anagrafica.component';



@NgModule({
  declarations: [
    ColorPickerComponent,
    RateComponent,
    MyInputComponent,
    AnagraficaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    ColorPickerComponent,
    RateComponent,
    MyInputComponent,
    AnagraficaComponent
  ]
})
export class SharedModule {
  static forRoot(config: any): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [

      ]
    }
  }
}

