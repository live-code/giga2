import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ValidationErrors,
  Validator,
  Validators
} from "@angular/forms";

@Component({
  selector: 'gig-anagrafica',
  template: `
    <h1>Anagrafica</h1>
    <form [formGroup]="form">
      <input
        class="form-control"
        [ngClass]="{
           'is-invalid': form.get('name')?.invalid
        }"
        type="text" placeholder="name" formControlName="name">
      <input type="text" placeholder="surname" formControlName="surname">
    </form>
  `,
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: AnagraficaComponent, multi: true},
    {provide: NG_VALIDATORS, useExisting: AnagraficaComponent, multi: true},
  ]
})
export class AnagraficaComponent implements ControlValueAccessor, Validator {

  form = this.fb.group({
    name: ['Fab', Validators.required],
    surname: 'Bio'
  })
  constructor(private fb: FormBuilder) {}

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
    this.form.patchValue(obj);
  }

  validate(control: AbstractControl): ValidationErrors | null {
    return this.form.invalid ?
      {
        invalid: true,
      } : null;
  }

}
