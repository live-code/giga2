import { Component, OnInit } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'gig-rate',
  template: `

    <i
      *ngFor="let star of [null, null, null, null, null]; let i = index"
      class="fa"
      [ngClass]="{
        'fa-star': i < value,
        'fa-star-o': i >= value
      }"
      [style.opacity]="isDisabled ? 0.4 : 1"
      (click)="clickHandler(i + 1)"
    ></i>
  `,
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: RateComponent, multi: true}
  ]
})
export class RateComponent implements ControlValueAccessor {
  value!: number
  isDisabled: boolean = false;
  onChange!: (newRate: number) => void;
  onTouched!: () => void;

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(rate: number): void {
    this.value = rate;
  }

  clickHandler(newRate: number) {
    this.value = newRate;
    this.onTouched();
    this.onChange(newRate)
  }

  setDisabledState?(isDisabled: boolean): void {
    console.log('sono disabiitato?', isDisabled)
    this.isDisabled = isDisabled;
  }
}
