import {Component, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'gig-color-picker',
  template: `
    <div class="d-flex">
      <div
        *ngFor="let c of colors"
        [style.background-color]="c"
        [style.border]="c === val ? '2px solid black' : null"
        style="width: 30px; height: 30px"
        (click)="clickHandler(c)"
      >
      </div>
    </div>
  `,
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: ColorPickerComponent, multi: true}
  ]
})
export class ColorPickerComponent implements ControlValueAccessor {
  val: string = 'abc';
  onChange!: (color: string) => void;
  onTouched!: () => void;

  @Input() colors = ['red', 'blue', 'green'];

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.val = color;
  }

  clickHandler(c: string) {
    this.onTouched();
    this.onChange(c);
    this.val = c;
  }
}
