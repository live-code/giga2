import {Component, Injector, Input, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ValidationErrors,
  Validator
} from "@angular/forms";

@Component({
  selector: 'gig-my-input',
  template: `
    <label for="">{{label}}</label>
    <input
      class="form-control"
      [ngClass]="{'is-invalid': ngControl.invalid}"
      type="text" [formControl]="input"
      (blur)="onTouched()"
    >
  `,
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: MyInputComponent, multi: true},
    {provide: NG_VALIDATORS, useExisting: MyInputComponent, multi: true},
  ]
})
export class MyInputComponent implements ControlValueAccessor, Validator {
  @Input() label: string = '';
  @Input() alphanumeric: boolean = false;

  input = new FormControl()
  onTouched!: () => void;
  ngControl!: NgControl;

  constructor(private inj: Injector) {}

  ngOnInit() {
    this.ngControl = this.inj.get(NgControl)
  }

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text);
  }

  validate(c: AbstractControl): ValidationErrors | null {
    if (
      this.alphanumeric &&
      c.value &&
      !c.value.match(ALPHA_NUMERIC_REGEX)
    ) {
      return { alphaNumeric: true }
    }
    return null;
  }

}

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
