import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./core/auth/auth.guard";

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'page1', canActivate:[AuthGuard], loadChildren: () => import('./features/page1/page1.module').then(m => m.Page1Module) },
  { path: 'page2', loadChildren: () => import('./features/page2/page2.module').then(m => m.Page2Module) },
  { path: '', redirectTo: 'page1', pathMatch: 'full' },
  { path: 'forms1', loadChildren: () => import('./features/forms1/forms1.module').then(m => m.Forms1Module) },
  { path: 'forms2', loadChildren: () => import('./features/forms2/forms2.module').then(m => m.Forms2Module) },
  { path: 'forms3', loadChildren: () => import('./features/forms3/forms3.module').then(m => m.Forms3Module) },
  { path: 'forms4', loadChildren: () => import('./features/forms4/forms4.module').then(m => m.Forms4Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
