import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page1RoutingModule } from './page1-routing.module';
import { Page1Component } from './page1.component';
import {CoreModule} from "../../core/core.module";
import {LogService} from "../../core/services/log.service";
import {GoogleLogService} from "../../core/services/google-log.service";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Page1Component
  ],
  imports: [
    CommonModule,
    Page1RoutingModule,
    CoreModule,
    SharedModule
  ],
  providers: [
    { provide: LogService, useClass: GoogleLogService}
  ],

})
export class Page1Module { }
