import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Auth} from "../../model/auth";
import {catchError, of} from "rxjs";
import {AuthService} from "../../core/auth/auth.service";
import {PanelComponent} from "../../core/panel.component";
import {LogService} from "../../core/services/log.service";

@Component({
  selector: 'gig-page1',
  template: `
    <p (click)="log.inc()">
      page1 works! {{log.value}}
    </p>

    <button
      *gigIfRoleIs="'moderator'"
      class="btn btn-primary">Moderator</button>

    <button
      *gigIfRoleIs="'admin'"
      class="btn btn-primary">ADMIN FEATURES</button>

  `,
  styles: [
  ]
})
export class Page1Component implements OnInit {

  constructor(
    private http: HttpClient,
    public authService: AuthService,
    public log: LogService
  ) {
    this.http.get<Auth>('http://localhost:3000/products')
      .subscribe({
        next(val) { console.log('val', val)},
        error(err) { console.log('ERRORE!', err)}
      })
  }

  ngOnInit(): void {
  }



}
