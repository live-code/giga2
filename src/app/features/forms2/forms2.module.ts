import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Forms2RoutingModule } from './forms2-routing.module';
import { Forms2Component } from './forms2.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Forms2Component
  ],
  imports: [
    CommonModule,
    Forms2RoutingModule,
    ReactiveFormsModule
  ]
})
export class Forms2Module { }
