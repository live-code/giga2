import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {pairwise, startWith} from "rxjs";

@Component({
  selector: 'gig-forms2',
  template: `

    <form [formGroup]="form">
      <input type="text" formControlName="orderNumber">


      <div *ngIf="step === 1">
        <h1>
          <i class="fa fa-check" *ngIf="form.get('user')?.valid"></i>
          Anagrafica
        </h1>

        <div formGroupName="user">
          <input type="text" formControlName="name"
                 class="form-control"
                 [ngClass]="{'is-invalid': form.get('user')?.get('name')?.invalid}"
          >
          <input type="text" formControlName="surname" class="form-control">
        </div>

        <button (click)="step = 2" [disabled]="form.get('user')?.invalid">Next</button>
      </div>


      <div *ngIf="step === 2">
        <h1>
          <i class="fa fa-check" *ngIf="form.get('car')?.valid"></i>
          Car info
        </h1>
        <div formGroupName="car">
          <input type="text" formControlName="brand"
                 class="form-control"
                 [ngClass]="{'is-invalid': form.get('car')?.get('brand')?.invalid}"
          >
          <input type="text" formControlName="name" class="form-control">
        </div>
        <button [disabled]="form.invalid">SAVE</button>

      </div>


      <pre>{{form.value | json}}</pre>

    </form>
  `,
})
export class Forms2Component implements OnInit {
  step = 1;

  form = this.fb.group({
    orderNumber: ['', Validators.required],
    user: this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
    }),
    car: this.fb.group({
      brand: ['', Validators.required],
      name: ['', Validators.required],
    }),

  })


  constructor(private fb: FormBuilder) {

//    this.form.get('user')?.get('name')?.disable();
    console.log(this.form.value)
    console.log(this.form.getRawValue())

    this.form.patchValue({
      orderNumber: 'ciao',
      user: {
        name: 'ciao',
        surname: 'ciao'
      }
    })
  }

  ngOnInit(): void {
  }

}
