import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, UntypedFormBuilder, ValidationErrors, Validators} from "@angular/forms";
import {catchError, debounceTime, distinctUntilChanged, filter, of, switchMap} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'gig-forms1',
  template: `

    <form [formGroup]="form">
      <input type="checkbox"
             formControlName="isCompany"> Are you a Company?

      <input
        type="text" class="form-control"
        placeholder="Enter Name"
        formControlName="name"
        [ngClass]="{'is-invalid': form.get('name')?.invalid && form.dirty }"
      >

      <input
        type="text" class="form-control"
        [placeholder]="form.get('isCompany')?.value ? 'Enter Your VAT' : 'Enter your CF'"
        formControlName="vatCF"
        [ngClass]="{'is-invalid': form.get('vatCF')?.invalid && form.dirty }"
      >

      <div *ngIf="form.get('vat')?.errors as err">
        <div *ngIf="err['required']">Campo Obbligatorio</div>
        <div *ngIf="err['alphaNumeric']">No Symbols</div>
      </div>

      <button [disabled]="form.invalid">SAVE</button>
    </form>

    <pre>Touched {{form.touched | json}}</pre>
    <pre>Dirty {{form.dirty | json}}</pre>
    <pre>Valid {{form.valid | json}}</pre>
    <pre>{{form.value | json}}</pre>
  `,
})
export class Forms1Component {

  form = this.fb.nonNullable.group({
    isCompany: true,
    name: ['', [Validators.required, Validators.minLength(3)]],
    vatCF: ['']
  })

  constructor(private fb: FormBuilder) {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if(isCompany) {
          this.setCompany()
        } else {
          this.setUser()
        }
        this.form.get('vatCF')?.updateValueAndValidity()
      })

    this.setCompany()
  }

  setCompany() {
    this.form.get('vatCF')?.setValidators([Validators.required, (c: FormControl) =>  vatCFValidator(c, 11)])
  }

  setUser() {
    this.form.get('vatCF')?.setValidators([Validators.required, (c: FormControl) =>  vatCFValidator(c, 16)])
  }

}


// =======

export function vatCFValidator(c: FormControl, requiredLength: number): ValidationErrors | null {
  if (c.value && c.value.length !== requiredLength) {
    return { vatCF: true }
  }
  return null
}





const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9])+$/;

export function alphaNumericValidator(c: FormControl): ValidationErrors | null {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphaNumeric: true }
  }
  return null
}
