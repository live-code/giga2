import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Page2RoutingModule } from './page2-routing.module';
import { Page2Component } from './page2.component';
import {CoreModule} from "../../core/core.module";
import {LogService} from "../../core/services/log.service";
import {GoogleLogService} from "../../core/services/google-log.service";
import {environment} from "../../../environments/environment";
import {FakeLogService} from "../../core/services/fake-log.service";


@NgModule({
  declarations: [
    Page2Component
  ],
  imports: [
    CommonModule,
    CoreModule,
    Page2RoutingModule
  ],
  providers: [
    { provide: LogService, useFactory: () => {
      return environment.production ?
          new GoogleLogService() : new FakeLogService()
    }}
  ],
})
export class Page2Module { }
