import { Component, OnInit } from '@angular/core';
import {LogService} from "../../core/services/log.service";

@Component({
  selector: 'gig-page2',
  template: `
    page2 works! {{log.value}}

    <gig-accordion>
      <gig-panel title="title1">body 1</gig-panel>
      <br>
      <gig-panel title="title2" >body 2</gig-panel>
    </gig-accordion>

    <gig-panel title="title1">body 1</gig-panel>
    <button (click)="log.inc()">+</button>

  `,

  styles: [
  ]
})
export class Page2Component implements OnInit {

  constructor(public log: LogService) { }

  ngOnInit(): void {
  }

}
