import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Forms4RoutingModule } from './forms4-routing.module';
import { Forms4Component } from './forms4.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    Forms4Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Forms4RoutingModule,
    SharedModule
  ]
})
export class Forms4Module { }
