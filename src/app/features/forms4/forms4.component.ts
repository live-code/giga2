import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'gig-forms4',
  template: `
    <form [formGroup]="form">
      <!--<input type="text" formControlName="name">-->
      <gig-my-input
        label="Your Name"
        [alphanumeric]="true"
        formControlName="name"></gig-my-input>

      <gig-color-picker formControlName="bg"></gig-color-picker>
      <gig-rate formControlName="rate"></gig-rate>

      <br>
      <gig-anagrafica formControlName="anagrafica"></gig-anagrafica>

    </form>

    <hr>
    ANAGRAFICA IS VALID?
    <pre>{{form.get('anagrafica')?.errors | json}}</pre>
    <pre>{{form.get('anagrafica')?.valid}}</pre>
  `,
})
export class Forms4Component{
  form = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    bg: ['', Validators.required],
    rate: 1,
    anagrafica: { name: 'a', surname: 'b '}
  })

  constructor(private fb: FormBuilder) {
    setTimeout(() => {
      this.form.patchValue({
        bg: 'blue',
        rate: 4,
        name: 'pippo',
        anagrafica: { name: '11', surname: '222'}
      })

      // this.form.get('rate')?.disable()
    }, 3000)


  }

}
