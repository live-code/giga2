import {Component, Injectable, OnInit} from '@angular/core';
import {AbstractControl, AsyncValidatorFn, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {interval, map, of, switchMap, timer} from "rxjs";

@Component({
  selector: 'gig-forms3',
  template: `
    <h1>Group Validator</h1>
    <form [formGroup]="form" (submit)="save()">
      <input formControlName="username" type="text" placeholder="username">
      <i class="fa fa-spinner" *ngIf="form.get('username')?.pending"></i>

      <h1>
        Passwords
        <i class="fa fa-check" *ngIf="form.get('passwords')?.valid"></i>
      </h1>
      <div formGroupName="passwords">
        <input formControlName="password1" type="text" placeholder="Password" autocomplete="off">
        <input formControlName="password2" type="text" placeholder="Repeat Password" autocomplete="off">

        <hr>
        <pre>{{form.get('passwords')?.errors | json}}</pre>
        <pre>{{form.get('passwords.password1')?.errors | json}}</pre>
        <pre>{{form.get('passwords.password2')?.errors | json}}</pre>
      </div>
      <button [disabled]="form.invalid || form.pending">SEND</button>
    </form>

    <pre>{{form.value | json}}</pre>
  `,
})
export class Forms3Component {
  form = this.fb.group({
    username: [
      '',
      [Validators.required, Validators.minLength(3)],
      [this.userValidator.checkUniqueName()]
    ],
    passwords: this.fb.group(
      {
        password1: ['b', [Validators.required]],
        password2: ['c', [Validators.required]],
      },
      {
        validators: passwordMatch('password1','password2')
      }
    )
  })

  constructor(private fb: FormBuilder , private userValidator: UserValidator) { }


  save() {
    console.log(this.form.value)
  }
}


export function passwordMatch(p1: string, p2: string): ValidatorFn {
  return (g: AbstractControl) => {
    const pass1 = g.get(p1)
    const pass2 = g.get(p2)

    if(pass1?.value !== pass2?.value) {
      pass2?.setErrors({ ...pass2?.errors, passNoMatch: true })
      return { passwordsDoesNOtMatch: true}
    }

    return null;
  };
}


// ==== user validator-ts
@Injectable({
  providedIn: 'root'
})
export class UserValidator {
  constructor(private http: HttpClient) {}

  checkUniqueName(): AsyncValidatorFn {
    return (control: AbstractControl) => {
      return timer(1000)
        .pipe(
          switchMap(() =>
            this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${control.value}`)
          ),
          map(res => res.length ? ({ userExist: true }) : null)
        )


    }
  }
}

