import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Forms3Component } from './forms3.component';

const routes: Routes = [{ path: '', component: Forms3Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Forms3RoutingModule { }
