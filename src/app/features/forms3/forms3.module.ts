import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Forms3RoutingModule } from './forms3-routing.module';
import { Forms3Component } from './forms3.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    Forms3Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Forms3RoutingModule
  ]
})
export class Forms3Module { }
