import {InjectionToken, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from "@angular/common/http";
import {AuthInterceptor} from "./core/auth/auth.interceptor";
import {LanguageService} from "./core/services/language.service";
import {SharedModule} from "./shared/shared.module";

export const DEFAULT_LANGUAGE = new InjectionToken<string>('defaultLanguage')

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
    SharedModule.forRoot({ theme: 'dark' })
  ],
  providers: [
    {
      provide: DEFAULT_LANGUAGE,
      useValue: 'XYZ'
    },
    {
      provide: LanguageService,
      useFactory: (http: HttpClient, defaultLang: string) => {
        return new LanguageService(http, defaultLang)
      },
      deps: [
        HttpClient,
        DEFAULT_LANGUAGE
      ]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    /*{
      provide: HTTP_INTERCEPTORS,
      useClass: AzureInterceptor,
      multi: true
    },*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

//
