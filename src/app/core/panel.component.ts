import {ChangeDetectionStrategy, Component, Input, OnInit, Optional} from '@angular/core';
import {AccordionComponent} from "./accordion.component";
import {LanguageService} from "./services/language.service";

@Component({
  selector: 'gig-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div>
      panel works! {{title}} - {{value}}
      <div>
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ],
})
export class PanelComponent implements OnInit {
  @Input() title: string = ''
  value = '';

  constructor(
    @Optional() public accordion: AccordionComponent,
    public languageService: LanguageService
  ) {
    console.log('---->', languageService.lang)
    if (!accordion) {
      this.value = 'no accordion'
    } else {
      this.value = accordion.value.toString()
    }
  }

  ngOnInit(): void {
  }

}
