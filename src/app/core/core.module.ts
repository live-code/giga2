import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar.component';
import {RouterModule} from "@angular/router";
import { IfLoggedDirective } from './auth/if-logged.directive';
import { PanelComponent } from './panel.component';
import { AccordionComponent } from './accordion.component';
import {IfRoleIsDirective} from "./auth/role.directive";



@NgModule({
  declarations: [
    NavbarComponent,
    IfLoggedDirective,
    IfRoleIsDirective,
    PanelComponent,
    AccordionComponent
  ],
  exports: [
    NavbarComponent,
    PanelComponent,
    IfRoleIsDirective,
    AccordionComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class CoreModule {


}
