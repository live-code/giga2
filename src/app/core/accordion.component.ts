import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'gig-accordion',
  template: `
    <div style="border: 2px solid blue; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements OnInit {
  @Input() value = 123;

  constructor() { }

  ngOnInit(): void {
  }

}
