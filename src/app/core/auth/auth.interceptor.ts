import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {catchError, first, iif, map, mergeMap, Observable, of, take, throwError} from 'rxjs';
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {}

  /*
  intercept2(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const tk = this.authService.auth$.getValue()?.token;
    if (tk) {
      const clone = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + tk
        }
      })
      return next.handle(clone)
    }
    return next.handle(request)
  }
   */
/*
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        mergeMap(tk => {
          if (tk) {
            const clone = request.clone({
              setHeaders: {
                Authorization: 'Bearer ' + tk
              }
            })
            return next.handle(clone)
          }
          return next.handle(request)
        })
      )
  }*/


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // console.log('auth interceptor')
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(tk => iif(
          () => !!tk && !request.url.includes('openweather'),
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + tk }})),
          next.handle(request),
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 403:
              case 404:
                alert('error lato server')
                break;

              default:
              case 401:
                this.router.navigateByUrl('login')
                break;

            }
          }
          return throwError(err)
        })
      )
  }
}
