import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {catchError, first, iif, map, mergeMap, Observable, of, take, throwError} from 'rxjs';
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";


@Injectable()
export class AzureInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        mergeMap(tk => iif(
          () => !!tk && request.url.includes('/azure.../'),
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + tk }})),
          next.handle(request),
        )),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch(err.status) {
              case 403:
              case 404:
                alert('error lato server')
                break;

              default:
              case 401:
                this.router.navigateByUrl('login')
                break;

            }
          }
          return throwError(err)
        })
      )
  }

}
