import {Directive, ElementRef, HostBinding, Renderer2, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "./auth.service";

@Directive({
  selector: '[gigIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private authService: AuthService,
    private templateRef: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
    this.authService.isLogged$
      .subscribe(val => {
        if (val) {
          view.createEmbeddedView(templateRef)
        } else {
          view.clear();
        }
      })
  }

}
