import {Directive, ElementRef, HostBinding, Input, Renderer2, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from "./auth.service";
import {Subject, Subscription, takeUntil} from "rxjs";

@Directive({
  selector: '[gigIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() gigIfRoleIs: 'moderator' | 'admin' | null = null;
  sub = new Subject();

  constructor(
    private authService: AuthService,
    private templateRef: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
  }

  ngOnInit() {
    this.authService.role$
      .pipe(
        takeUntil(this.sub)
      )
      .subscribe(role => {
        console.log('IF ROLE IS', role, this.gigIfRoleIs)
        if (role === this.gigIfRoleIs) {
          this.view.createEmbeddedView(this.templateRef)
        } else {
          this.view.clear();
        }
      })
  }

  ngOnDestroy() {
    this.sub.next(null);
    this.sub.complete();
  }

}
