import { Injectable } from '@angular/core';
import {BehaviorSubject, map, mergeMap, Observable, of, tap, withLatestFrom} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Auth} from "../../model/auth";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient, private router: Router) {
    const auth = localStorage.getItem('auth')
    if (auth) {
      this.auth$.next(JSON.parse(auth) as Auth)
    }
  }

  login(username: string, password: string) {
    this.http.get<Auth>('http://localhost:3000/login')
      .pipe(
        mergeMap(auth => this.http.get<any>('http://localhost:3000/profile', {
          headers: {
            Authorization: 'Bearer ' + auth.token
          }
        })
          .pipe( map(profile => ({profile, auth})) )
        )
      )
      .subscribe(res => {
        this.auth$.next(res.auth);
        this.router.navigateByUrl('page1');
        localStorage.setItem('auth', JSON.stringify(res.auth))
      })
  }


  logout() {
    this.auth$.next(null);
    localStorage.removeItem('auth')
    this.router.navigateByUrl('login');
  }

  get isLogged$(): Observable<boolean> {
    return this.auth$
      .pipe(
        map(val => !!val)
      )
  }


  get displayName$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(val => val?.displayName)
      )
  }

  get role$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(val => val?.role)
      )
  }

  get token$(): Observable<string | undefined> {
    return this.auth$
      .pipe(
        map(val => val?.token)
      )
  }

}

