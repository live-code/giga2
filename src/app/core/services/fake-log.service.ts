import { Injectable } from '@angular/core';
import {LogService} from "./log.service";

@Injectable({
  providedIn: 'root'
})
export class FakeLogService extends LogService {

  constructor() {
    console.log('fake log')
    super();
  }

  override inc() {
    console.log('fake inc')
    super.inc()
  }
}
