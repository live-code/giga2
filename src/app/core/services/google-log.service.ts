import { Injectable } from '@angular/core';
import {LogService} from "./log.service";

@Injectable({
  providedIn: 'root'
})
export class GoogleLogService extends LogService {

  constructor() {
    console.log('google log')
    super();
  }

  override inc() {
    console.log('google inc')
    super.inc()
  }
}
