import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {DEFAULT_LANGUAGE} from "../../app.module";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  lang = 'it'

  constructor(
    http: HttpClient,
    @Inject(DEFAULT_LANGUAGE) defaultLang: string
  ) {
    console.log('language srv', defaultLang)
    // this.lang = value;
  }

}
