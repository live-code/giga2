import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AuthService} from "../auth/auth.service";

interface User {
  id: number
}

@Component({
  selector: 'gig-navbar',
  template: `
    <div>
      <button class="btn btn-primary" routerLink="login">login</button>
      <button class="btn btn-primary" routerLink="page1">page1</button>
      <button class="btn btn-primary" routerLink="page2">page2</button>
      <button class="btn btn-primary" routerLink="forms1">forms1</button>
      <button class="btn btn-primary" routerLink="forms2">forms2</button>
      <button class="btn btn-primary" routerLink="forms3">forms3</button>
      <button class="btn btn-primary" routerLink="forms4">forms4</button>

      <button
        class="btn btn-primary"
        *gigIfLogged
        (click)="authService.logout()"
      >Logout</button>

      {{authService.displayName$ | async}}
    </div>
  `,
})
export class NavbarComponent  {

  constructor(public authService: AuthService) {
  }


}
