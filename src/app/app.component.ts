import { Component } from '@angular/core';

@Component({
  selector: 'gig-root',
  template: `
    <gig-navbar></gig-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'giga-2';
}
